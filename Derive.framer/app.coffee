# This imports all the layers for "PhoneScreens-01" into phonescreens01Layers23
AllLayers = Framer.Importer.load "imported/PhoneScreens-01"

# Make all the imported layers available on the root
for layerGroupName of AllLayers
  window[layerGroupName] = AllLayers[layerGroupName]

for layerGroupName of AllLayers
  AllLayers[layerGroupName].originalFrame = window[layerGroupName].frame

# Welcome to Framer
# A couple shortcut functions
Layer::fadeIn = ->
  this.animate
    properties: 
      opacity: 1
      x: this.originalFrame.x
    curve: 'ease-in-out'
    time: 0.25
    index: this.bringToFront()

Layer::fadeOut = ->
  this.animate
    properties: 
      opacity: 0
      x: this.x += 750
    curve: 'ease-in-out'
    time: 0.25
    index: this.sendToBack()

Layer::fadeInOnly = ->
  this.animate
    properties: 
      opacity: 1
    curve: 'ease-in-out'
    time: 0.1


Layer::fadeOutOnly = ->
  this.animate
    properties: 
      opacity: 0
    curve: 'ease-in-out'
    time: 0.1


#Hide Screens
FriendsScreen.x += 750
CreateDerive1.x += 750
CreateDerive2.x += 750
CreateDerive3.x += 750
CreateDerive4.x += 750
CreateDerive4a.x += 750
CreateDerive4b.x += 750
CreateDerive5.x += 750
Final.x += 750
FriendsScreen.opacity = 0
CreateDerive1.opacity = 0
CreateDerive2.opacity = 0
CreateDerive3.opacity = 0
CreateDerive4.opacity = 0
CreateDerive4a.opacity = 0
CreateDerive4b.opacity = 0
CreateDerive5.opacity = 0
Final.opacity = 0

#Set all clicks to opacity = 0
BusClick.opacity = 0
Back1.opacity = 0
Next1.opacity = 0
SelectSights1.opacity = 0
Next2.opacity = 0
Select2.opacity = 0
Next3.opacity = 0
Check2.opacity = 0
Check1.opacity = 0
Time4.opacity = 0
Next4b.opacity = 0
Next5.opacity = 0
SelectOffRoad.opacity = 0
SelectScenic.opacity = 0
BusUser.opacity = 0
SelectResident.opacity = 0


BusClick.on Events.TouchStart, -> 
  BusClick.fadeInOnly() if BusClick.opacity == 0
  BusClick.fadeOutOnly() if BusClick.opacity == 1
     
AllFriends.on Events.TouchStart, -> 
  MainScreen.fadeOut()
  FriendsScreen.fadeIn()

Back1.on Events.TouchStart, ->
  FriendsScreen.fadeOut()
  MainScreen.fadeIn()
  
CreateDerive.on Events.TouchStart, ->
  MainScreen.fadeOut()
  CreateDerive1.fadeIn()

SelectSights1.on Events.TouchStart, ->
  SelectSights1.fadeIn()

Next1.on Events.TouchStart, ->
  CreateDerive1.fadeOut()
  CreateDerive2.fadeIn()

Select2.on Events.TouchStart, ->
  Select2.fadeIn()

Next2.on Events.TouchStart, ->
  CreateDerive2.fadeOut()
  CreateDerive3.fadeIn()

Check2.on Events.TouchStart, ->
  Check2.fadeInOnly() if Check2.opacity == 0
  Check2.fadeOutOnly() if Check2.opacity == 1
  
Check1.on Events.TouchStart, ->
  Check1.fadeInOnly() if Check1.opacity == 0
  Check1.fadeOutOnly() if Check1.opacity == 1

Next3.on Events.TouchStart, ->
  CreateDerive3.fadeOut()
  CreateDerive4.fadeIn()

Time4.on Events.TouchStart, ->
  CreateDerive4.fadeOut()
  CreateDerive4a.fadeIn()

Overlay4a.on Events.TouchStart, ->
  CreateDerive4a.fadeOut()
  CreateDerive4b.fadeIn()

Next4b.on Events.TouchStart, ->
  CreateDerive4b.fadeOut()
  CreateDerive5.fadeIn()

SelectOffRoad.on Events.TouchStart, ->
  SelectOffRoad.fadeInOnly() if SelectOffRoad.opacity == 0
  SelectOffRoad.fadeOutOnly() if SelectOffRoad.opacity == 1
  
SelectScenic.on Events.TouchStart, ->
  SelectScenic.fadeInOnly() if SelectScenic.opacity == 0
  SelectScenic.fadeOutOnly() if SelectScenic.opacity == 1

BusUser.on Events.TouchStart, ->
  BusUser.fadeInOnly() if BusUser.opacity == 0
  BusUser.fadeOutOnly() if BusUser.opacity == 1

SelectResident.on Events.TouchStart, ->
  SelectResident.fadeInOnly() if SelectResident.opacity == 0
  SelectResident.fadeOutOnly() if SelectResident.opacity == 1

Next5.on Events.TouchStart, ->
  CreateDerive5.fadeOut()
  Final.fadeIn()
  Utils.delay 1, ->
  	Final.animate
  	  properties:
  	    index: Final.sendToBack()
  	    opacity: 0
  	    x: Final.x + 750
  	  time: 0
  	MainScreen.animate
  	  properties:
  	    index: MainScreen.bringToFront()
  	    opacity: 1
  	    x: MainScreen.originalFrame.x
  	    time: 0







    


  


