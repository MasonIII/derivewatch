window.__imported__ = window.__imported__ || {};
window.__imported__["PhoneScreens-02/layers.json.js"] = [
	{
		"id": 118,
		"name": "FriendsScreen",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 116,
				"name": "Back",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 114,
						"name": "<Group>-23",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-23.png",
							"frame": {
								"x": 0,
								"y": 1246,
								"width": 755,
								"height": 95
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 111,
								"name": "<Group>-22",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-22.png",
									"frame": {
										"x": 567,
										"y": 1274,
										"width": 47,
										"height": 47
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "2046687562"
							},
							{
								"id": 107,
								"name": "<Group>-21",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-21.png",
									"frame": {
										"x": 352,
										"y": 1272,
										"width": 52,
										"height": 51
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "2105006598"
							}
						],
						"modification": "1010759015"
					}
				],
				"modification": "2014218633"
			},
			{
				"id": 102,
				"name": "Background",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 755,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 99,
						"name": "<Group>-20",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-20.png",
							"frame": {
								"x": 223,
								"y": 106,
								"width": 16,
								"height": 8
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1574164265"
					},
					{
						"id": 95,
						"name": "<Group>-19",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-19.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 755,
								"height": 51
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 93,
								"name": "<Group>-18",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-18.png",
									"frame": {
										"x": 673,
										"y": 15,
										"width": 65,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "322470639"
							},
							{
								"id": 90,
								"name": "<Group>-17",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 88,
										"name": "<Clip Group>-2",
										"layerFrame": {
											"x": 634,
											"y": 11,
											"width": 16,
											"height": 27
										},
										"maskFrame": {
											"x": 634,
											"y": 12,
											"width": 16,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-2.png",
											"frame": {
												"x": 634,
												"y": 11,
												"width": 16,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "422542910"
									}
								],
								"modification": "1408101721"
							},
							{
								"id": 81,
								"name": "<Group>-16",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 79,
										"name": "<Clip Group>",
										"layerFrame": {
											"x": 579,
											"y": 12,
											"width": 26,
											"height": 26
										},
										"maskFrame": {
											"x": 580,
											"y": 12,
											"width": 25,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>.png",
											"frame": {
												"x": 580,
												"y": 12,
												"width": 25,
												"height": 26
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "845808864"
									}
								],
								"modification": "1419062794"
							},
							{
								"id": 73,
								"name": "<Group>-15",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-15.png",
									"frame": {
										"x": 527,
										"y": 12,
										"width": 29,
										"height": 26
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "318717127"
							}
						],
						"modification": "2078833402"
					},
					{
						"id": 69,
						"name": "<Group>-14",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-14.png",
							"frame": {
								"x": 0,
								"y": 1246,
								"width": 755,
								"height": 95
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 66,
								"name": "<Group>-13",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-13.png",
									"frame": {
										"x": 566,
										"y": 1274,
										"width": 48,
										"height": 47
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "538228712"
							},
							{
								"id": 62,
								"name": "<Group>-12",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-12.png",
									"frame": {
										"x": 351,
										"y": 1272,
										"width": 52,
										"height": 51
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "14339135"
							}
						],
						"modification": "20002193"
					},
					{
						"id": 56,
						"name": "<Group>-11",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-11.png",
							"frame": {
								"x": 149,
								"y": 91,
								"width": 47,
								"height": 33
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "290832627"
					},
					{
						"id": 52,
						"name": "<Group>-10",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-10.png",
							"frame": {
								"x": 639,
								"y": 438,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1234034298"
					},
					{
						"id": 48,
						"name": "<Group>-9",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-9.png",
							"frame": {
								"x": 639,
								"y": 707,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1384583322"
					},
					{
						"id": 44,
						"name": "<Group>-8",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-8.png",
							"frame": {
								"x": 639,
								"y": 825,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "499338083"
					},
					{
						"id": 40,
						"name": "<Group>-7",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-7.png",
							"frame": {
								"x": 639,
								"y": 1179,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1097438555"
					},
					{
						"id": 36,
						"name": "<Group>-6",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-6.png",
							"frame": {
								"x": 639,
								"y": 320,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1897077090"
					},
					{
						"id": 32,
						"name": "<Group>-5",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-5.png",
							"frame": {
								"x": 639,
								"y": 556,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "2073287925"
					},
					{
						"id": 28,
						"name": "<Group>-4",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-4.png",
							"frame": {
								"x": 639,
								"y": 1061,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "23924648"
					},
					{
						"id": 24,
						"name": "<Group>-3",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-3.png",
							"frame": {
								"x": 639,
								"y": 943,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "880710686"
					},
					{
						"id": 20,
						"name": "<Group>-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-2.png",
							"frame": {
								"x": 639,
								"y": 202,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1097283701"
					},
					{
						"id": 16,
						"name": "<Group>",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>.png",
							"frame": {
								"x": 639,
								"y": 825,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1860072732"
					}
				],
				"modification": "432043901"
			}
		],
		"modification": "115800615"
	}
]