window.__imported__ = window.__imported__ || {};
window.__imported__["PhoneScreens-01/layers.json.js"] = [
	{
		"id": 810,
		"name": "Final",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": {
			"path": "images/Final.png",
			"frame": {
				"x": 0,
				"y": 3,
				"width": 755,
				"height": 1338
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1400288343"
	},
	{
		"id": 807,
		"name": "CreateDerive5",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 806,
				"name": "Next5",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Next5.png",
					"frame": {
						"x": 534,
						"y": 1187,
						"width": 72,
						"height": 21
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "762748138"
			},
			{
				"id": 803,
				"name": "SelectOffRoad",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/SelectOffRoad.png",
					"frame": {
						"x": 558,
						"y": 487,
						"width": 25,
						"height": 26
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2124611520"
			},
			{
				"id": 800,
				"name": "SelectScenic",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/SelectScenic.png",
					"frame": {
						"x": 710,
						"y": 488,
						"width": 26,
						"height": 26
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2124611517"
			},
			{
				"id": 797,
				"name": "BusUser",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/BusUser.png",
					"frame": {
						"x": 383,
						"y": 410,
						"width": 20,
						"height": 20
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2124611492"
			},
			{
				"id": 794,
				"name": "SelectResident",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/SelectResident.png",
					"frame": {
						"x": 381,
						"y": 562,
						"width": 352,
						"height": 178
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2124611489"
			},
			{
				"id": 791,
				"name": "Content5",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Content5.png",
					"frame": {
						"x": 1,
						"y": 217,
						"width": 754,
						"height": 1012
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2100412832"
			},
			{
				"id": 745,
				"name": "Background5",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background5.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 755,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 744,
						"name": "<Group>-93",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-93.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 755,
								"height": 48
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 743,
								"name": "<Group>-92",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-92.png",
									"frame": {
										"x": 673,
										"y": 11,
										"width": 65,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "2121930202"
							},
							{
								"id": 740,
								"name": "<Group>-91",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 739,
										"name": "<Clip Group>-17",
										"layerFrame": {
											"x": 633,
											"y": 8,
											"width": 17,
											"height": 27
										},
										"maskFrame": {
											"x": 634,
											"y": 8,
											"width": 15,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-17.png",
											"frame": {
												"x": 633,
												"y": 8,
												"width": 17,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "2103187722"
									}
								],
								"modification": "1674491400"
							},
							{
								"id": 731,
								"name": "<Group>-90",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 730,
										"name": "<Clip Group>-16",
										"layerFrame": {
											"x": 579,
											"y": 8,
											"width": 26,
											"height": 27
										},
										"maskFrame": {
											"x": 580,
											"y": 10,
											"width": 24,
											"height": 24
										},
										"image": {
											"path": "images/<Clip Group>-16.png",
											"frame": {
												"x": 579,
												"y": 8,
												"width": 26,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "600491949"
									}
								],
								"modification": "261870772"
							},
							{
								"id": 723,
								"name": "<Group>-89",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-89.png",
									"frame": {
										"x": 526,
										"y": 8,
										"width": 30,
										"height": 26
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "2121900664"
							}
						],
						"modification": "1399760830"
					},
					{
						"id": 718,
						"name": "<Group>-88",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-88.png",
							"frame": {
								"x": 0,
								"y": 1243,
								"width": 755,
								"height": 98
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 716,
								"name": "<Group>-87",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-87.png",
									"frame": {
										"x": 566,
										"y": 1270,
										"width": 47,
										"height": 48
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "366794411"
							},
							{
								"id": 712,
								"name": "<Group>-86",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-86.png",
									"frame": {
										"x": 351,
										"y": 1268,
										"width": 52,
										"height": 52
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "366030517"
							}
						],
						"modification": "1545596114"
					},
					{
						"id": 705,
						"name": "<Group>-85",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-85.png",
							"frame": {
								"x": 151,
								"y": 88,
								"width": 117,
								"height": 34
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "2121900629"
					}
				],
				"modification": "83216100"
			}
		],
		"modification": "1545703253"
	},
	{
		"id": 699,
		"name": "CreateDerive4b",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 698,
				"name": "Next4b",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Next4b.png",
					"frame": {
						"x": 536,
						"y": 1184,
						"width": 72,
						"height": 21
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1842084433"
			},
			{
				"id": 695,
				"name": "Content4b",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Content4b.png",
					"frame": {
						"x": 3,
						"y": 215,
						"width": 752,
						"height": 1011
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1978294385"
			},
			{
				"id": 675,
				"name": "Background4b",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background4b.png",
					"frame": {
						"x": 3,
						"y": 0,
						"width": 752,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 674,
						"name": "<Group>-84",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-84.png",
							"frame": {
								"x": 3,
								"y": 0,
								"width": 752,
								"height": 48
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 673,
								"name": "<Group>-83",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-83.png",
									"frame": {
										"x": 678,
										"y": 12,
										"width": 66,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1269148987"
							},
							{
								"id": 670,
								"name": "<Group>-82",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 669,
										"name": "<Clip Group>-15",
										"layerFrame": {
											"x": 639,
											"y": 8,
											"width": 16,
											"height": 27
										},
										"maskFrame": {
											"x": 639,
											"y": 9,
											"width": 16,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-15.png",
											"frame": {
												"x": 639,
												"y": 8,
												"width": 16,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "737622437"
									}
								],
								"modification": "212013695"
							},
							{
								"id": 661,
								"name": "<Group>-81",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 660,
										"name": "<Clip Group>-14",
										"layerFrame": {
											"x": 584,
											"y": 8,
											"width": 26,
											"height": 27
										},
										"maskFrame": {
											"x": 585,
											"y": 9,
											"width": 25,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-14.png",
											"frame": {
												"x": 585,
												"y": 9,
												"width": 25,
												"height": 26
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1298175682"
									}
								],
								"modification": "1215514270"
							},
							{
								"id": 653,
								"name": "<Group>-80",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-80.png",
									"frame": {
										"x": 532,
										"y": 8,
										"width": 29,
										"height": 27
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1269148928"
							}
						],
						"modification": "1915869092"
					},
					{
						"id": 648,
						"name": "<Group>-79",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-79.png",
							"frame": {
								"x": 2,
								"y": 1243,
								"width": 753,
								"height": 98
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 646,
								"name": "<Group>-78",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-78.png",
									"frame": {
										"x": 571,
										"y": 1270,
										"width": 48,
										"height": 48
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "161026103"
							},
							{
								"id": 642,
								"name": "<Group>-77",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-77.png",
									"frame": {
										"x": 356,
										"y": 1268,
										"width": 52,
										"height": 52
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1279297598"
							}
						],
						"modification": "148405134"
					},
					{
						"id": 635,
						"name": "<Group>-76",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-76.png",
							"frame": {
								"x": 156,
								"y": 88,
								"width": 117,
								"height": 34
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1269148870"
					}
				],
				"modification": "1691013417"
			}
		],
		"modification": "1811720651"
	},
	{
		"id": 629,
		"name": "CreateDerive4a",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 628,
				"name": "Overlay4a",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Overlay4a.png",
					"frame": {
						"x": 0,
						"y": 7,
						"width": 755,
						"height": 1243
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "829495777"
			},
			{
				"id": 622,
				"name": "Content4a",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Content4a.png",
					"frame": {
						"x": 0,
						"y": 213,
						"width": 755,
						"height": 1012
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "839351129"
			},
			{
				"id": 607,
				"name": "Background4a",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background4a.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 755,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 606,
						"name": "<Group>-75",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-75.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 755,
								"height": 51
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 605,
								"name": "<Group>-74",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-74.png",
									"frame": {
										"x": 674,
										"y": 15,
										"width": 65,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1243997235"
							},
							{
								"id": 602,
								"name": "<Group>-73",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 601,
										"name": "<Clip Group>-13",
										"layerFrame": {
											"x": 634,
											"y": 11,
											"width": 17,
											"height": 27
										},
										"maskFrame": {
											"x": 635,
											"y": 12,
											"width": 15,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-13.png",
											"frame": {
												"x": 634,
												"y": 11,
												"width": 17,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1228573462"
									}
								],
								"modification": "1555167508"
							},
							{
								"id": 593,
								"name": "<Group>-72",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 592,
										"name": "<Clip Group>-12",
										"layerFrame": {
											"x": 579,
											"y": 12,
											"width": 27,
											"height": 26
										},
										"maskFrame": {
											"x": 580,
											"y": 13,
											"width": 25,
											"height": 25
										},
										"image": {
											"path": "images/<Clip Group>-12.png",
											"frame": {
												"x": 580,
												"y": 12,
												"width": 26,
												"height": 26
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "978734770"
									}
								],
								"modification": "1627579193"
							},
							{
								"id": 585,
								"name": "<Group>-71",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-71.png",
									"frame": {
										"x": 527,
										"y": 12,
										"width": 30,
										"height": 26
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1243997272"
							}
						],
						"modification": "2105088358"
					},
					{
						"id": 580,
						"name": "<Group>-70",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-70.png",
							"frame": {
								"x": 0,
								"y": 1246,
								"width": 755,
								"height": 95
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 578,
								"name": "<Group>-69",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-69.png",
									"frame": {
										"x": 567,
										"y": 1274,
										"width": 47,
										"height": 47
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1782708025"
							},
							{
								"id": 574,
								"name": "<Group>-68",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-68.png",
									"frame": {
										"x": 352,
										"y": 1272,
										"width": 51,
										"height": 51
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1547669823"
							}
						],
						"modification": "1329607027"
					},
					{
						"id": 567,
						"name": "<Group>-67",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-67.png",
							"frame": {
								"x": 152,
								"y": 92,
								"width": 116,
								"height": 33
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1244026810"
					}
				],
				"modification": "429805247"
			}
		],
		"modification": "132884376"
	},
	{
		"id": 561,
		"name": "CreateDerive4",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 560,
				"name": "Time4",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Time4.png",
					"frame": {
						"x": 549,
						"y": 425,
						"width": 28,
						"height": 28
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "329346816"
			},
			{
				"id": 557,
				"name": "Content4",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Content4.png",
					"frame": {
						"x": 2,
						"y": 217,
						"width": 753,
						"height": 1011
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1699638994"
			},
			{
				"id": 537,
				"name": "Background4",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background4.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 755,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 536,
						"name": "<Group>-66",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-66.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 755,
								"height": 51
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 535,
								"name": "<Group>-65",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-65.png",
									"frame": {
										"x": 674,
										"y": 15,
										"width": 65,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "309118541"
							},
							{
								"id": 532,
								"name": "<Group>-64",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 531,
										"name": "<Clip Group>-11",
										"layerFrame": {
											"x": 635,
											"y": 11,
											"width": 16,
											"height": 27
										},
										"maskFrame": {
											"x": 635,
											"y": 12,
											"width": 15,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-11.png",
											"frame": {
												"x": 635,
												"y": 11,
												"width": 16,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1396949149"
									}
								],
								"modification": "21746386"
							},
							{
								"id": 523,
								"name": "<Group>-63",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 522,
										"name": "<Clip Group>-10",
										"layerFrame": {
											"x": 580,
											"y": 12,
											"width": 26,
											"height": 26
										},
										"maskFrame": {
											"x": 581,
											"y": 12,
											"width": 25,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-10.png",
											"frame": {
												"x": 580,
												"y": 12,
												"width": 26,
												"height": 26
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1621187750"
									}
								],
								"modification": "1552150211"
							},
							{
								"id": 515,
								"name": "<Group>-62",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-62.png",
									"frame": {
										"x": 528,
										"y": 12,
										"width": 29,
										"height": 26
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "309088998"
							}
						],
						"modification": "1473133375"
					},
					{
						"id": 510,
						"name": "<Group>-61",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-61.png",
							"frame": {
								"x": 0,
								"y": 1246,
								"width": 755,
								"height": 95
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 508,
								"name": "<Group>-60",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-60.png",
									"frame": {
										"x": 567,
										"y": 1274,
										"width": 48,
										"height": 47
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "896080449"
							},
							{
								"id": 504,
								"name": "<Group>-59",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-59.png",
									"frame": {
										"x": 352,
										"y": 1272,
										"width": 52,
										"height": 51
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "369084531"
							}
						],
						"modification": "1524256678"
					},
					{
						"id": 497,
						"name": "<Group>-58",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-58.png",
							"frame": {
								"x": 152,
								"y": 92,
								"width": 117,
								"height": 33
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "309088960"
					}
				],
				"modification": "786023368"
			}
		],
		"modification": "1374465234"
	},
	{
		"id": 491,
		"name": "CreateDerive3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 490,
				"name": "Next3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Next3.png",
					"frame": {
						"x": 535,
						"y": 1186,
						"width": 72,
						"height": 21
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1136197041"
			},
			{
				"id": 487,
				"name": "Check2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Check2.png",
					"frame": {
						"x": 553,
						"y": 513,
						"width": 34,
						"height": 33
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "191227582"
			},
			{
				"id": 484,
				"name": "Check1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Check1.png",
					"frame": {
						"x": 553,
						"y": 400,
						"width": 34,
						"height": 33
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "191227580"
			},
			{
				"id": 481,
				"name": "Content3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Content3.png",
					"frame": {
						"x": 2,
						"y": 217,
						"width": 753,
						"height": 1011
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 469,
						"name": "<Group>-57",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-57.png",
							"frame": {
								"x": 641,
								"y": 610,
								"width": 85,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1825886709"
					},
					{
						"id": 465,
						"name": "<Group>-56",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-56.png",
							"frame": {
								"x": 641,
								"y": 880,
								"width": 85,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1590848507"
					},
					{
						"id": 461,
						"name": "<Group>-55",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-55.png",
							"frame": {
								"x": 641,
								"y": 997,
								"width": 85,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "289457109"
					},
					{
						"id": 457,
						"name": "<Group>-54",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-54.png",
							"frame": {
								"x": 641,
								"y": 492,
								"width": 85,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "524495312"
					},
					{
						"id": 453,
						"name": "<Group>-53",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-53.png",
							"frame": {
								"x": 641,
								"y": 728,
								"width": 85,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "2112843211"
					},
					{
						"id": 449,
						"name": "<Group>-52",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-52.png",
							"frame": {
								"x": 641,
								"y": 374,
								"width": 85,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "310030775"
					},
					{
						"id": 445,
						"name": "<Group>-51",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-51.png",
							"frame": {
								"x": 641,
								"y": 997,
								"width": 85,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1038774461"
					}
				],
				"modification": "111059821"
			},
			{
				"id": 434,
				"name": "Background3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background3.png",
					"frame": {
						"x": 1,
						"y": 0,
						"width": 754,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 433,
						"name": "<Group>-50",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-50.png",
							"frame": {
								"x": 1,
								"y": 0,
								"width": 754,
								"height": 51
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 432,
								"name": "<Group>-49",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-49.png",
									"frame": {
										"x": 676,
										"y": 15,
										"width": 66,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "190304026"
							},
							{
								"id": 429,
								"name": "<Group>-48",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 428,
										"name": "<Clip Group>-9",
										"layerFrame": {
											"x": 637,
											"y": 11,
											"width": 16,
											"height": 27
										},
										"maskFrame": {
											"x": 637,
											"y": 12,
											"width": 16,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-9.png",
											"frame": {
												"x": 637,
												"y": 11,
												"width": 16,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1134621957"
									}
								],
								"modification": "1337233123"
							},
							{
								"id": 420,
								"name": "<Group>-47",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 419,
										"name": "<Clip Group>-8",
										"layerFrame": {
											"x": 582,
											"y": 12,
											"width": 26,
											"height": 26
										},
										"maskFrame": {
											"x": 583,
											"y": 13,
											"width": 25,
											"height": 25
										},
										"image": {
											"path": "images/<Clip Group>-8.png",
											"frame": {
												"x": 583,
												"y": 12,
												"width": 25,
												"height": 26
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1544574078"
									}
								],
								"modification": "1812005374"
							},
							{
								"id": 412,
								"name": "<Group>-46",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-46.png",
									"frame": {
										"x": 530,
										"y": 12,
										"width": 30,
										"height": 26
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "190303962"
							}
						],
						"modification": "545950806"
					},
					{
						"id": 407,
						"name": "<Group>-45",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-45.png",
							"frame": {
								"x": 1,
								"y": 1246,
								"width": 754,
								"height": 95
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 405,
								"name": "<Group>-44",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-44.png",
									"frame": {
										"x": 569,
										"y": 1274,
										"width": 48,
										"height": 47
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "392585595"
							},
							{
								"id": 401,
								"name": "<Group>-43",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-43.png",
									"frame": {
										"x": 354,
										"y": 1272,
										"width": 52,
										"height": 51
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "433728185"
							}
						],
						"modification": "833541074"
					},
					{
						"id": 394,
						"name": "<Group>-42",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-42.png",
							"frame": {
								"x": 154,
								"y": 92,
								"width": 117,
								"height": 33
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "190303900"
					}
				],
				"modification": "1183293080"
			}
		],
		"modification": "287969052"
	},
	{
		"id": 388,
		"name": "CreateDerive2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 387,
				"name": "Next2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Next2.png",
					"frame": {
						"x": 531,
						"y": 1186,
						"width": 72,
						"height": 21
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2001641183"
			},
			{
				"id": 384,
				"name": "Select2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Select2.png",
					"frame": {
						"x": 658,
						"y": 740,
						"width": 43,
						"height": 44
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2001641181"
			},
			{
				"id": 381,
				"name": "Content2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Content2.png",
					"frame": {
						"x": 0,
						"y": 218,
						"width": 755,
						"height": 1010
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "397216565"
			},
			{
				"id": 355,
				"name": "Background2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background2.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 755,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 354,
						"name": "<Group>-41",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-41.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 755,
								"height": 51
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 353,
								"name": "<Group>-40",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-40.png",
									"frame": {
										"x": 674,
										"y": 15,
										"width": 65,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1981412968"
							},
							{
								"id": 350,
								"name": "<Group>-39",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 349,
										"name": "<Clip Group>-7",
										"layerFrame": {
											"x": 634,
											"y": 11,
											"width": 17,
											"height": 27
										},
										"maskFrame": {
											"x": 635,
											"y": 12,
											"width": 15,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-7.png",
											"frame": {
												"x": 634,
												"y": 11,
												"width": 17,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1274373646"
									}
								],
								"modification": "501660322"
							},
							{
								"id": 341,
								"name": "<Group>-38",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 340,
										"name": "<Clip Group>-6",
										"layerFrame": {
											"x": 579,
											"y": 12,
											"width": 27,
											"height": 26
										},
										"maskFrame": {
											"x": 580,
											"y": 13,
											"width": 25,
											"height": 25
										},
										"image": {
											"path": "images/<Clip Group>-6.png",
											"frame": {
												"x": 580,
												"y": 12,
												"width": 26,
												"height": 26
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1897613092"
									}
								],
								"modification": "1892982677"
							},
							{
								"id": 333,
								"name": "<Group>-37",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-37.png",
									"frame": {
										"x": 527,
										"y": 12,
										"width": 30,
										"height": 26
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1981412933"
							}
						],
						"modification": "1176252206"
					},
					{
						"id": 328,
						"name": "<Group>-36",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-36.png",
							"frame": {
								"x": 0,
								"y": 1246,
								"width": 755,
								"height": 95
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 326,
								"name": "<Group>-35",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-35.png",
									"frame": {
										"x": 567,
										"y": 1274,
										"width": 47,
										"height": 47
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1296427945"
							},
							{
								"id": 322,
								"name": "<Group>-34",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-34.png",
									"frame": {
										"x": 352,
										"y": 1272,
										"width": 51,
										"height": 51
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1061389743"
							}
						],
						"modification": "1878335103"
					},
					{
						"id": 315,
						"name": "<Group>-33",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-33.png",
							"frame": {
								"x": 152,
								"y": 92,
								"width": 116,
								"height": 33
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1981412900"
					}
				],
				"modification": "597623829"
			}
		],
		"modification": "618016137"
	},
	{
		"id": 309,
		"name": "CreateDerive1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 308,
				"name": "Next1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Next1.png",
					"frame": {
						"x": 533,
						"y": 1182,
						"width": 72,
						"height": 21
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1944019545"
			},
			{
				"id": 305,
				"name": "SelectSights1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/SelectSights1.png",
					"frame": {
						"x": 659,
						"y": 505,
						"width": 44,
						"height": 43
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1944019547"
			},
			{
				"id": 302,
				"name": "Content1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Content1.png",
					"frame": {
						"x": 0,
						"y": 215,
						"width": 755,
						"height": 1010
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1098802896"
			},
			{
				"id": 281,
				"name": "Background1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background1.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 755,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 280,
						"name": "<Group>-32",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-32.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 755,
								"height": 48
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 279,
								"name": "<Group>-31",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-31.png",
									"frame": {
										"x": 674,
										"y": 11,
										"width": 65,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1944287671"
							},
							{
								"id": 276,
								"name": "<Group>-30",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 275,
										"name": "<Clip Group>-5",
										"layerFrame": {
											"x": 634,
											"y": 8,
											"width": 17,
											"height": 27
										},
										"maskFrame": {
											"x": 635,
											"y": 8,
											"width": 15,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-5.png",
											"frame": {
												"x": 634,
												"y": 8,
												"width": 17,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "717876496"
									}
								],
								"modification": "179692589"
							},
							{
								"id": 267,
								"name": "<Group>-29",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 266,
										"name": "<Clip Group>-4",
										"layerFrame": {
											"x": 579,
											"y": 8,
											"width": 27,
											"height": 27
										},
										"maskFrame": {
											"x": 581,
											"y": 9,
											"width": 24,
											"height": 25
										},
										"image": {
											"path": "images/<Clip Group>-4.png",
											"frame": {
												"x": 580,
												"y": 8,
												"width": 26,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1192146405"
									}
								],
								"modification": "136987196"
							},
							{
								"id": 259,
								"name": "<Group>-28",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-28.png",
									"frame": {
										"x": 527,
										"y": 8,
										"width": 30,
										"height": 26
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1944942818"
							}
						],
						"modification": "173702883"
					},
					{
						"id": 254,
						"name": "<Group>-27",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-27.png",
							"frame": {
								"x": 0,
								"y": 1243,
								"width": 755,
								"height": 98
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 252,
								"name": "<Group>-26",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-26.png",
									"frame": {
										"x": 567,
										"y": 1270,
										"width": 47,
										"height": 48
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "684571697"
							},
							{
								"id": 248,
								"name": "<Group>-25",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-25.png",
									"frame": {
										"x": 352,
										"y": 1268,
										"width": 52,
										"height": 52
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "485529434"
							}
						],
						"modification": "1793579310"
					},
					{
						"id": 241,
						"name": "<Group>-24",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-24.png",
							"frame": {
								"x": 152,
								"y": 88,
								"width": 116,
								"height": 34
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1944942852"
					}
				],
				"modification": "1669184467"
			}
		],
		"modification": "486520211"
	},
	{
		"id": 235,
		"name": "FriendsScreen",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 234,
				"name": "Back1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 233,
						"name": "<Group>-23",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-23.png",
							"frame": {
								"x": 0,
								"y": 1246,
								"width": 755,
								"height": 95
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 231,
								"name": "<Group>-22",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-22.png",
									"frame": {
										"x": 567,
										"y": 1274,
										"width": 47,
										"height": 47
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1019618939"
							},
							{
								"id": 227,
								"name": "<Group>-21",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-21.png",
									"frame": {
										"x": 352,
										"y": 1272,
										"width": 52,
										"height": 51
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1254657141"
							}
						],
						"modification": "1794814946"
					}
				],
				"modification": "280552738"
			},
			{
				"id": 220,
				"name": "Background0",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background0.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 755,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 218,
						"name": "<Group>-20",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-20.png",
							"frame": {
								"x": 223,
								"y": 106,
								"width": 16,
								"height": 8
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "2016691261"
					},
					{
						"id": 214,
						"name": "<Group>-19",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-19.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 755,
								"height": 51
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 213,
								"name": "<Group>-18",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-18.png",
									"frame": {
										"x": 673,
										"y": 15,
										"width": 65,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "871370332"
							},
							{
								"id": 210,
								"name": "<Group>-17",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 209,
										"name": "<Clip Group>-2-2",
										"layerFrame": {
											"x": 634,
											"y": 11,
											"width": 16,
											"height": 27
										},
										"maskFrame": {
											"x": 634,
											"y": 12,
											"width": 16,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-2-2.png",
											"frame": {
												"x": 634,
												"y": 11,
												"width": 16,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "933936096"
									}
								],
								"modification": "325988132"
							},
							{
								"id": 201,
								"name": "<Group>-16",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 200,
										"name": "<Clip Group>-3",
										"layerFrame": {
											"x": 579,
											"y": 12,
											"width": 26,
											"height": 26
										},
										"maskFrame": {
											"x": 580,
											"y": 12,
											"width": 25,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-3.png",
											"frame": {
												"x": 580,
												"y": 12,
												"width": 25,
												"height": 26
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "2074320263"
									}
								],
								"modification": "412981339"
							},
							{
								"id": 193,
								"name": "<Group>-15",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-15.png",
									"frame": {
										"x": 527,
										"y": 12,
										"width": 29,
										"height": 26
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1242676506"
							}
						],
						"modification": "729898168"
					},
					{
						"id": 188,
						"name": "<Group>-14",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-14.png",
							"frame": {
								"x": 0,
								"y": 1246,
								"width": 755,
								"height": 95
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 186,
								"name": "<Group>-13",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-13.png",
									"frame": {
										"x": 566,
										"y": 1274,
										"width": 48,
										"height": 47
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1157006849"
							},
							{
								"id": 182,
								"name": "<Group>-12",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-12.png",
									"frame": {
										"x": 351,
										"y": 1272,
										"width": 52,
										"height": 51
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "921968647"
							}
						],
						"modification": "313808561"
					},
					{
						"id": 175,
						"name": "<Group>-11-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-11-2.png",
							"frame": {
								"x": 149,
								"y": 91,
								"width": 47,
								"height": 33
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1242676472"
					},
					{
						"id": 171,
						"name": "<Group>-10-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-10-2.png",
							"frame": {
								"x": 639,
								"y": 438,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1513244225"
					},
					{
						"id": 167,
						"name": "<Group>-9-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-9-2.png",
							"frame": {
								"x": 639,
								"y": 707,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1663451575"
					},
					{
						"id": 163,
						"name": "<Group>-8-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-8-2.png",
							"frame": {
								"x": 639,
								"y": 825,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1185684385"
					},
					{
						"id": 159,
						"name": "<Group>-7-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-7-2.png",
							"frame": {
								"x": 639,
								"y": 1179,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "516171903"
					},
					{
						"id": 155,
						"name": "<Group>-6-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-6-2.png",
							"frame": {
								"x": 639,
								"y": 320,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "281133701"
					},
					{
						"id": 151,
						"name": "<Group>-5-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-5-2.png",
							"frame": {
								"x": 639,
								"y": 556,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "46095499"
					},
					{
						"id": 147,
						"name": "<Group>-4-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-4-2.png",
							"frame": {
								"x": 639,
								"y": 1061,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1834210117"
					},
					{
						"id": 143,
						"name": "<Group>-3-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-3-2.png",
							"frame": {
								"x": 639,
								"y": 943,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "2069248319"
					},
					{
						"id": 139,
						"name": "<Group>-2-2",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-2-2.png",
							"frame": {
								"x": 639,
								"y": 202,
								"width": 86,
								"height": 85
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1990680775"
					},
					{
						"id": 135,
						"name": "<Group>-11",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-11.png",
							"frame": {
								"x": 639,
								"y": 825,
								"width": 86,
								"height": 86
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "20340442"
					}
				],
				"modification": "1129166698"
			}
		],
		"modification": "369294317"
	},
	{
		"id": 101,
		"name": "MainScreen",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 755,
			"height": 1341
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 106,
				"name": "MainSettings",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/MainSettings.png",
					"frame": {
						"x": 39,
						"y": 93,
						"width": 39,
						"height": 27
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1112990783"
			},
			{
				"id": 108,
				"name": "AllFriends",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/AllFriends.png",
					"frame": {
						"x": 321,
						"y": 771,
						"width": 31,
						"height": 21
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1113675854"
			},
			{
				"id": 97,
				"name": "CreateDerive",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/CreateDerive.png",
					"frame": {
						"x": 394,
						"y": 1155,
						"width": 338,
						"height": 56
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "853745685"
			},
			{
				"id": 110,
				"name": "BusClick",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/BusClick.png",
					"frame": {
						"x": 556,
						"y": 1112,
						"width": 13,
						"height": 13
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1135989246"
			},
			{
				"id": 92,
				"name": "Settings",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Settings.png",
					"frame": {
						"x": 378,
						"y": 748,
						"width": 377,
						"height": 463
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 75,
						"name": "<Group>-10",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-10.png",
							"frame": {
								"x": 511,
								"y": 1042,
								"width": 160,
								"height": 15
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1472008512"
					}
				],
				"modification": "932454963"
			},
			{
				"id": 59,
				"name": "FriendsList",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/FriendsList.png",
					"frame": {
						"x": 0,
						"y": 748,
						"width": 379,
						"height": 450
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "495310402"
			},
			{
				"id": 48,
				"name": "Map",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Map.png",
					"frame": {
						"x": 0,
						"y": 175,
						"width": 755,
						"height": 575
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "924499364"
			},
			{
				"id": 45,
				"name": "Background",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 755,
					"height": 1341
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 755,
						"height": 1341
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 43,
						"name": "<Group>-9",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-9.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 755,
								"height": 49
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 41,
								"name": "<Group>-8",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-8.png",
									"frame": {
										"x": 675,
										"y": 12,
										"width": 65,
										"height": 21
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "843438151"
							},
							{
								"id": 38,
								"name": "<Group>-7",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 36,
										"name": "<Clip Group>-2",
										"layerFrame": {
											"x": 635,
											"y": 9,
											"width": 17,
											"height": 27
										},
										"maskFrame": {
											"x": 636,
											"y": 9,
											"width": 15,
											"height": 26
										},
										"image": {
											"path": "images/<Clip Group>-2.png",
											"frame": {
												"x": 635,
												"y": 9,
												"width": 17,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "108033112"
									}
								],
								"modification": "1915269301"
							},
							{
								"id": 29,
								"name": "<Group>-6",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": null,
								"imageType": null,
								"children": [
									{
										"id": 27,
										"name": "<Clip Group>",
										"layerFrame": {
											"x": 581,
											"y": 9,
											"width": 26,
											"height": 27
										},
										"maskFrame": {
											"x": 582,
											"y": 11,
											"width": 24,
											"height": 24
										},
										"image": {
											"path": "images/<Clip Group>.png",
											"frame": {
												"x": 581,
												"y": 9,
												"width": 26,
												"height": 27
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "1310118310"
									}
								],
								"modification": "407198663"
							},
							{
								"id": 21,
								"name": "<Group>-5",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-5.png",
									"frame": {
										"x": 528,
										"y": 9,
										"width": 30,
										"height": 27
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "837003231"
							}
						],
						"modification": "232782126"
					},
					{
						"id": 17,
						"name": "<Group>-4",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>-4.png",
							"frame": {
								"x": 0,
								"y": 1244,
								"width": 755,
								"height": 97
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 14,
								"name": "<Group>-3",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-3.png",
									"frame": {
										"x": 568,
										"y": 1271,
										"width": 47,
										"height": 48
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "557119821"
							},
							{
								"id": 10,
								"name": "<Group>-2",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 755,
									"height": 1341
								},
								"maskFrame": null,
								"image": {
									"path": "images/<Group>-2.png",
									"frame": {
										"x": 353,
										"y": 1269,
										"width": 52,
										"height": 52
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1739652440"
							}
						],
						"modification": "1833507696"
					},
					{
						"id": 4,
						"name": "<Group>",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 755,
							"height": 1341
						},
						"maskFrame": null,
						"image": {
							"path": "images/<Group>.png",
							"frame": {
								"x": 153,
								"y": 89,
								"width": 117,
								"height": 34
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "810072069"
					}
				],
				"modification": "1914402216"
			}
		],
		"modification": "99478724"
	}
]