window.__imported__ = window.__imported__ || {};
window.__imported__["WatchScreens-01/layers.json.js"] = [
	{
		"id": 14,
		"name": "InviteScreen",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 16,
				"name": "Text",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Text.png",
					"frame": {
						"x": 9,
						"y": 70,
						"width": 303,
						"height": 66
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "238361818"
			},
			{
				"id": 11,
				"name": "No",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/No.png",
					"frame": {
						"x": 187,
						"y": 198,
						"width": 98,
						"height": 98
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "239285345"
			},
			{
				"id": 8,
				"name": "Yes",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Yes.png",
					"frame": {
						"x": 35,
						"y": 198,
						"width": 99,
						"height": 98
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "260347453"
			},
			{
				"id": 5,
				"name": "Unchecked",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Unchecked.png",
					"frame": {
						"x": 35,
						"y": 198,
						"width": 249,
						"height": 99
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1122545070"
			},
			{
				"id": 18,
				"name": "Background0",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background0.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 320,
						"height": 400
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "265054552"
			}
		],
		"modification": "1965959766"
	}
]