window.__imported__ = window.__imported__ || {};
window.__imported__["WatchScreens/layers.json.js"] = [
	{
		"id": 69,
		"name": "NavScreen4",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 68,
				"name": "Radar4",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Radar4.png",
					"frame": {
						"x": 0,
						"y": 60,
						"width": 320,
						"height": 340
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1958797933"
			},
			{
				"id": 65,
				"name": "Time4",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Time4.png",
					"frame": {
						"x": 104,
						"y": 28,
						"width": 114,
						"height": 23
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1958827718"
			},
			{
				"id": 62,
				"name": "Background4",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background4.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 320,
						"height": 400
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1958827721"
			}
		],
		"modification": "364033100"
	},
	{
		"id": 58,
		"name": "NavScreen3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 57,
				"name": "Radar3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Radar3.png",
					"frame": {
						"x": 53,
						"y": 95,
						"width": 109,
						"height": 110
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "341381803"
			},
			{
				"id": 53,
				"name": "Time3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Time3.png",
					"frame": {
						"x": 88,
						"y": 28,
						"width": 145,
						"height": 23
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1506027453"
			},
			{
				"id": 50,
				"name": "Background3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background3.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 320,
						"height": 400
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "557789188"
			}
		],
		"modification": "470026051"
	},
	{
		"id": 46,
		"name": "NavScreen2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 45,
				"name": "Radar2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Radar2.png",
					"frame": {
						"x": 41,
						"y": 79,
						"width": 124,
						"height": 126
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1269975629"
			},
			{
				"id": 41,
				"name": "Time2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Time2.png",
					"frame": {
						"x": 87,
						"y": 28,
						"width": 146,
						"height": 23
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2053562485"
			},
			{
				"id": 38,
				"name": "Background2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background2.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 320,
						"height": 400
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "620433278"
			}
		],
		"modification": "993719886"
	},
	{
		"id": 34,
		"name": "NavScreen1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 33,
				"name": "Time1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Time1.png",
					"frame": {
						"x": 86,
						"y": 28,
						"width": 147,
						"height": 23
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "695021647"
			},
			{
				"id": 30,
				"name": "Radar1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Radar1.png",
					"frame": {
						"x": 32,
						"y": 72,
						"width": 133,
						"height": 133
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "383638111"
			},
			{
				"id": 24,
				"name": "Background1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background1.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 320,
						"height": 400
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "695021676"
			}
		],
		"modification": "1327389766"
	},
	{
		"id": 14,
		"name": "InviteScreen",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 320,
			"height": 400
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 16,
				"name": "Text",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Text.png",
					"frame": {
						"x": 9,
						"y": 70,
						"width": 303,
						"height": 66
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "676966778"
			},
			{
				"id": 11,
				"name": "No",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/No.png",
					"frame": {
						"x": 187,
						"y": 198,
						"width": 98,
						"height": 98
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "676877341"
			},
			{
				"id": 8,
				"name": "Yes",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Yes.png",
					"frame": {
						"x": 35,
						"y": 198,
						"width": 99,
						"height": 98
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1822950069"
			},
			{
				"id": 5,
				"name": "Unchecked",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Unchecked.png",
					"frame": {
						"x": 35,
						"y": 198,
						"width": 249,
						"height": 99
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1607458809"
			},
			{
				"id": 18,
				"name": "Background0",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 320,
					"height": 400
				},
				"maskFrame": null,
				"image": {
					"path": "images/Background0.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 320,
						"height": 400
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "676877313"
			}
		],
		"modification": "953306731"
	}
]