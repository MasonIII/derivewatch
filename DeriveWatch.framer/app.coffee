# This imports all the layers for "WatchScreens" into watchscreensLayers3
AllLayers = Framer.Importer.load "imported/WatchScreens"

# Make all the imported layers available on the root
for layerGroupName of AllLayers
  window[layerGroupName] = AllLayers[layerGroupName]

for layerGroupName of AllLayers
  AllLayers[layerGroupName].originalFrame = window[layerGroupName].frame

# Welcome to Framer
# A couple shortcut functions
Layer::fadeIn = ->
  this.animate
    properties: 
      opacity: 1
      x: this.originalFrame.x
    curve: 'ease-in-out'
    time: 0.25
    index: this.bringToFront()

Layer::fadeOut = ->
  this.animate
    properties: 
      opacity: 0
      x: this.x += 750
    curve: 'ease-in-out'
    time: 0.25
    index: this.sendToBack()

Radar1.animate
  properties:
    originX: 1
    originY: 1
    rotation: Utils.randomNumber(0, 360)
Radar2.animate
  properties:
    originX: 1
    originY: 1
    rotation: Utils.randomNumber(0, 360)
Radar3.animate
  properties:
    originX: 1
    originY: 1
    rotation: Utils.randomNumber(0, 360)

#Initialize Layers
NavScreen1.opacity = 0
NavScreen2.opacity = 0
NavScreen3.opacity = 0
NavScreen4.opacity = 0
NavScreen1.x += 320
NavScreen2.x += 320
NavScreen3.x += 320
NavScreen4.x += 320

#Initialize Buttons
Yes.opacity = 0.3
No.opacity = 0.3
Unchecked.opacity = 0.5

Yes.on Events.TouchStart, ->
  Yes.fadeIn()
  Unchecked.fadeOut()
  No.fadeOut()
  Utils.delay 1, ->
  	InviteScreen.fadeOut()
  	NavScreen1.fadeIn()

No.on Events.TouchStart, ->
  No.fadeIn()
  Unchecked.fadeOut()
  Yes.fadeOut()
  Utils.delay 1, ->
    InviteScreen.fadeOut()

Radar1.on Events.TouchStart, ->
  Radar1.animate
    properties:
      originX: 1
      originY: 1
      rotation: Utils.randomNumber(0, 360)
      
Time1.on Events.TouchStart, ->
  NavScreen1.fadeOut()
  NavScreen2.fadeIn()

Radar2.on Events.TouchStart, ->
  Radar2.animate
    properties:
      originX: 1
      originY: 1
      rotation: Utils.randomNumber(0, 360)
      
Time2.on Events.TouchStart, ->
  NavScreen2.fadeOut()
  NavScreen3.fadeIn()

Radar3.on Events.TouchStart, ->
  Radar3.animate
    properties:
      originX: 1
      originY: 1
      rotation: Utils.randomNumber(0, 360)
      
Time3.on Events.TouchStart, ->
  NavScreen3.fadeOut()
  NavScreen4.fadeIn()
  
NavScreen4.on Events.TouchStart, ->
  Utils.delay 2, ->
  	NavScreen4.animate
  	  properties:
  	    opacity: 0
  	  time: 2



